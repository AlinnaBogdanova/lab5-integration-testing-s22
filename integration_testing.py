import itertools
import requests
import tqdm

KEY = 'AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w'
EMAIL = 'a.bogdanova@innopolis.university'
TYPES = ['budget', 'luxury']
TARIFFS = ['minute', 'fixed_price']

combinations = [[TYPES[0], TARIFFS[0]], [TYPES[1], TARIFFS[0]], [TYPES[1], TARIFFS[0]]]

first_request = 'https://script.google.com/macros/s/{}/exec?service=getSpec&email={}'.format(KEY, EMAIL)

x = requests.get(first_request)
print(x.content)

# response:
# Here is InnoCar Specs:
# Budet car price per minute = 17
# Luxury car price per minute = 50
# Fixed price per km = 12
# Allowed deviations in % = 6
# Inno discount in % = 9

# cppem - car price per minute
prices_per_minute = [17, 50]
# fixed proce per km
fpp_km = 12 # don't know where to use
DEVIATION = 0.06
DISCOUNT = 0.09
INVALID = 'Invalid Request'

def valid_answer(type, tariff, distance = None, planned_distance = None, time = None, planned_time = None, discount = 'no'):
    # invalid case (`The fixed_price plan is available for budget cars only.`)
    if tariff == TARIFFS[1] and type == TYPES[1]:
        return INVALID
    if not tariff in TARIFFS or not type in TYPES:
        return INVALID
    vals = [time, distance, planned_time, planned_distance]
    for val in vals:
        if (not val is None) and val < 0:
            return INVALID
    # if time <= 0 or distance <= 0 or planned_time <= 0 or planned_distance <= 0:
    #     return INVALID
    # if not discount in ['yes', 'no']:
    #     return INVALID
    # check deviation -> change tariff
    distanceIsOk = (distance is not None) and (planned_distance is not None) and ((planned_distance == 0 and distance > DEVIATION) or abs(distance / planned_distance - 1) > DEVIATION)
    timeIsOk = (time is not None) and (planned_time is not None) and ((planned_time == 0 and time > DEVIATION) or abs(time / planned_time - 1) > DEVIATION)

    if tariff == TARIFFS[1] and (distanceIsOk or timeIsOk):
        tariff = TARIFFS[0]
    if tariff == TARIFFS[0]: # minute
        if time is None:
            return INVALID
        ppm = prices_per_minute[0] if type == TYPES[0] else prices_per_minute[1]
        price = ppm * time

    elif tariff == TARIFFS[1]: # fixed
        if planned_time is not None:
            ppm = prices_per_minute[0] # for budget only
            price = planned_time * ppm
        elif planned_distance is not None:
            price = fpp_km * planned_distance
        else:
            return INVALID
    else:
        return INVALID
    if (discount == 'yes'):
        price -= price * DISCOUNT
    converted_to_int = int(price) if int(price) == price else price
    result = '{"price":'+str(converted_to_int)+'}'
    return result


def main_request(type, tariff, distance, planned_distance, time, planned_time, discount):
    link = 'https://script.google.com/macros/s/{key}/exec?service=calculatePrice&email={email}&type={type}&plan={tariff}&distance={distance}&planned_distance={planned_distance}&time={time}&planned_time={planned_time}&inno_discount={discount}'
    formatted_link = link.format(key = KEY, email = EMAIL,
                                 type = type, tariff = tariff,
                                 distance = distance,
                                 planned_distance = planned_distance, time = time,
                                 planned_time = planned_time, discount = discount)
    return requests.get(formatted_link)


types = [*TYPES]
tariffs = [*TARIFFS]
big_val = 10000
distances = [big_val]
planned_distances = [*distances,
                     big_val + DEVIATION * big_val,
                     big_val - DEVIATION * big_val,
                     big_val - DEVIATION * big_val - 0.1,
                     big_val - DEVIATION * big_val + 0.1,
                     big_val + DEVIATION * big_val - 0.1,
                     big_val + DEVIATION * big_val + 0.1]
big_time = 1000
times = [big_time]
planned_times = [*distances,
                 big_time + DEVIATION * big_time,
                 big_time - DEVIATION * big_time,
                 big_time - DEVIATION * big_time - 0.1,
                 big_time - DEVIATION * big_time + 0.1,
                 big_time + DEVIATION * big_time - 0.1,
                 big_time + DEVIATION * big_time + 0.1]

discounts = ['no', 'yes']

output = ''
output += '## BVA table\n\n'
output += '| Parameter | Values | \n'
output += '|-----------|--------| \n'
output += '| tariff | {} | \n'.format(', '.join([ str(x) for x in tariffs] + ['bla-bla', 'MINUTE']))
output += '| type | {} | \n'.format(', '.join([ str(x) for x in types] + ['bla-bla', 'BUDGET']))
output += '| distance | {} | \n'.format(', '.join([ str(x) for x in distances] + ['-0.1', '0']))
output += '| planned_distance | {} | \n'.format(', '.join([ str(x) for x in planned_distances] + ['-0.01', '0']))
output += '| time | {} | \n'.format(', '.join([ str(x) for x in times] + ['-0.1', '0']))
output += '| planned_time | {} | \n'.format(', '.join([ str(x) for x in planned_times] + ['-0.3', '0']))
output += '| discount | {} | \n'.format(', '.join([ str(x) for x in discounts] + ['bla-bla', 'YES', 'NO']))

output += '\n\n'

invalid_tests = [
    ('bla-bla', tariffs[0], distances[0], planned_distances[0], times[0], planned_times[0], discounts[1]),
    ('BUDGET', tariffs[0], distances[0], planned_distances[0], times[0], planned_times[0], discounts[1]),
    (types[0], tariffs[0], distances[0], planned_distances[0], times[0], planned_times[0], discounts[1]),
    (types[0], tariffs[0], distances[0], planned_distances[0], times[0], planned_times[0], discounts[0]),
    (types[1], tariffs[1], distances[0], planned_distances[0], times[0], planned_times[0], discounts[1]),
    (types[0], 'bla-bla', distances[0], planned_distances[0], times[0], planned_times[0], discounts[1]),
    (types[0], 'MINUTE', distances[0], planned_distances[0], times[0], planned_times[0], discounts[1]),
    (types[0], tariffs[0], distances[0], planned_distances[0], times[0], planned_times[0], 'bla-bla'),
    (types[0], tariffs[0], distances[0], planned_distances[0], times[0], planned_times[0], 'YES'),
    (types[0], tariffs[0], distances[0], planned_distances[0], times[0], planned_times[0], 'NO'),
    (types[0], tariffs[0], - 0.1, planned_distances[0], times[0], planned_times[0], discounts[1]),
    (types[0], tariffs[0], 0, planned_distances[0], times[0], planned_times[0], discounts[1]),
    (types[0], tariffs[0], distances[0], - 0.01, times[0], planned_times[0], discounts[1]),
    (types[0], tariffs[0], distances[0], 0, times[0], planned_times[0], discounts[1]),
    (types[0], tariffs[0], distances[0], planned_distances[0], -0.1, planned_times[0], discounts[1]),
    (types[0], tariffs[0], distances[0], planned_distances[0], 0, planned_times[0], discounts[1]),
    (types[0], tariffs[0], distances[0], planned_distances[0], times[0], - 0.3, discounts[1]),
    (types[0], tariffs[0], distances[0], planned_distances[0], times[0], 0, discounts[1]),
]

output += '## Test cases Block 1\n\n'
output += '| Valid | Actual Result | Expected result | Type | Tariff | Distance | Planned distance | Time Planned time | Discount | \n'
output += '|-------|---------------|-----------------|------|--------|----------|------------------|-------------------|----------| \n'

for (type, tariff, distance, planned_distance, time, planned_time, discount) in tqdm.tqdm(invalid_tests):

    actual_result = main_request(type, tariff, distance, planned_distance, time, planned_time, discount).content.decode('utf-8')
    target_result = valid_answer(type, tariff, distance, planned_distance, time, planned_time, discount)
    if (actual_result != target_result):
        print(actual_result, target_result, '\t', type, tariff, distance, planned_distance, time, planned_time, discount)
    output += '| **{}** | {} | {} | {} | {} | {} | {} | {} | {} | {} |\n'.format(actual_result != target_result,
                                                                                 actual_result, target_result, type,
                                                                                 tariff, distance, planned_distance,
                                                                                 time, planned_time, discount)

print('all tests')
# 392 tests \_('_')_/
for (type, tariff, distance, planned_distance, time, planned_time, discount) in tqdm.tqdm(list(itertools.product(types, tariffs, distances, planned_distances, times, planned_times, discounts))):

    actual_result = main_request(type, tariff, distance, planned_distance, time, planned_time, discount).content.decode('utf-8')
    target_result = valid_answer(type, tariff, distance, planned_distance, time, planned_time, discount)
    if (actual_result != target_result):
        print(actual_result, target_result, '\t', type, tariff, distance, planned_distance, time, planned_time, discount)
    output += '| **{}** | {} | {} | {} | {} | {} | {} | {} | {} | {} |\n'.format(actual_result != target_result,
                                                                                 actual_result, target_result, type,
                                                                                 tariff, distance, planned_distance,
                                                                              time, planned_time, discount)

print(output)
with open('Notes.md', 'r') as file:
    file.write(output)
